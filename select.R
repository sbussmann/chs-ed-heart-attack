##############################################################################
# Project: CHS ED (Heart Related)
# Date: 9/15/2014
# Author: Sam Bussmann
# Description: Import data from SQLS
# Notes: Draw all community data -- then do a merge to ID patients
##############################################################################

source("functions.R")

cmty_area<-cmty_gather(c(106,122,123,124,126))

save(cmty_area,file="cmty_area.RData")
load("cmty_area.RData")


### Duplicates across groups?
dups<-dupsBetweenGroups(cmty_area[,c("familyid","ClientName")],cmty_area$ClientName)
sum(dups) #If zero, then no dups across service groups

pts<-pts_gather(c(106,122,123,124,126))
save(pts,file="pts.RData")
#load("pts.RData")

summary(pts$admitdate)
hist(as.numeric(pts$admitdate))

### Get Most profitable insurance for each encounter
## The below ended up not being necessary since none of the patients appeared to have a second insurance
## across the same encounter id
# sql<-odbcConnect("IrmSqls")
# 
# system.time(
#   ins<-as.data.frame(sqlQuery(sql,
#                               paste0("SELECT distinct
#                               pe.PatientEnounterID as PatientEncounterID,
#                               cp.CommunityPersonID,
#                               i.Name as Payer,
#                               p.Name as PayerType
#                               FROM ", dbname, ".dbo.PatientEncounter pe
#                               left join ", dbname, ".dbo.CommunityPerson cp on cp.communitypersonid = pe.PatientID
#                               left join ", dbname, ".dbo.insurance i on pe.insuranceid = i.insuranceid
#                               left join ", dbname, ".dbo.payertype p on pe.PatientEncounterPayerTypeID = p.PayerTypeID")
#                               ,errors=T,stringsAsFactors = F))
# )
# 
# dim(ins)[1]
# length(unique(ins$PatientEncounterID))


### Select the codes of interest to make up the target group

# prec<-read.table("CPT_codes.txt",colClasses="character")

## Codes are increasing in specificity with the length of the code

# pts[,"pt_flag"]<-rep(0,length(pts$ServiceDisplayText))

# pts[(  pts$ServiceDisplayText %in% c("722.10","722.52","724.2")
#                 & pts$ServiceType =="ICD-9"),"pt_flag"]<-1


# 
# t<-table(pts$ProcedureDisplayText,useNA="ifany")
# t[order(t,decreasing=T)][1:20]
# 
smk<-grep("AMI ",pts$ServiceDesc)
smk<-(substr(pts$ServiceDisplayText,1,3) %in% 410)
t2<-table(pts$ServiceDesc[smk])
t2<-table(pts$ServiceDisplayText[smk],pts$PatientType[smk])
sum(t2)
### Oly 193 former smokers so ignore

### Alternative is lung cancer patients

pts[,"pt_flag"]<-rep(0,length(pts$ServiceDisplayText))
pts[(pts$PatientType =="Emergency"),"pt_flag"]<-1

pts[,"pt_heart"]<-rep(0,length(pts$ServiceDisplayText))
pts[ ((substr(pts$ServiceDisplayText,1,3) == 410) & (pts$ServiceType =="ICD-9")) |
       ((pts$ServiceDisplayText %in% 280:285) & (pts$ServiceType =="MS-DRG"))
    ,"pt_heart"]<-1 ### This is just the heart attack codes



#table(pts[(pts$pt_flag==1),"ServiceDesc"])
table(pts[(pts$pt_flag==1),"ServiceGroup"])
#table(pts[(pts$pt_flag==1),"ServiceDisplayText"],pts[(pts$pt_flag==1),"ServiceType"])
#table(pts[(pts$pt_flag==1),"ServiceDisplayText"],pts[(pts$pt_flag==1),"ClientName"])

### Then Deduplicate by CommunityPersonID 
## First create payer table with unique persons
pts_payer<-pts[order(pts$CommunityPersonID, -as.numeric(pts$admitdate)),]
pts_payer<-pts_payer[!duplicated(pts_payer$CommunityPersonID),c("CommunityPersonID","PayerType","Payer","ClientName")]

## Now sum over encounters of same patient

pt_agg<-by(pts$pt_flag,pts$CommunityPersonID,sum)
pt_agg[(pt_agg>0)]<-1

pt_agg2<-by(pts$pt_heart,pts$CommunityPersonID,sum)
pt_agg2[(pt_agg2>0)]<-1

pts$TotalCost_ED<-ifelse(pts$pt_flag==1,pts$TotalCost,0)

pt_cost<-by(pts$TotalCost_ED,pts$CommunityPersonID,sum)

## Choose most visited facility
pts_fac<-tapply(pts$facility,pts$CommunityPersonID,Mode)

## Create DS with unique patients and patient data
pts_unq<-data.frame(CommunityPersonID=as.integer(names(pt_agg)),pt_flag=as.integer(pt_agg),
                    pt_heart=as.integer(pt_agg2),
                    PayerType=pts_payer$PayerType,
                    Payer=pts_payer$Payer,
                    Cost=as.numeric(pt_cost),Facility=pts_fac,ClientName_pt=pts_payer$ClientName)

### Then Merge in with the community data

system.time(
  cmty_wtgt<-merge(cmty_area,pts_unq,by=c("CommunityPersonID"), all.x=TRUE)
)

### Set patient flag to zero for non matches
cmty_wtgt$pt_flag[is.na(cmty_wtgt$pt_flag)]<-0
cmty_wtgt$pt_heart[is.na(cmty_wtgt$pt_heart)]<-0

table(cmty_wtgt$pt_flag[cmty_wtgt$ComHHFlag==1 & cmty_wtgt$ComPerFlag==1])

### Handle blank values in character variable

for (i in 1:dim(cmty_wtgt)[2]){
  if (is.character(cmty_wtgt[,i])) cmty_wtgt[[i]][cmty_wtgt[[i]] %in% c(" ","")]<-" BLANK"
}

save(cmty_wtgt,file="cmty_wtgt.RData")

#load("cmty_wtgt.RData")

