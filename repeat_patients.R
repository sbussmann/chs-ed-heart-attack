### What percent of patients are repeaters?


### Take half of data

summary(pts$admitdate)

pts_lw<-pts[pts$admitdate > "2012-01-30",]

pts_lw_per<-unique(pts_lw["CommunityPersonID"])

pts_prev<-pts[pts$admitdate <= "2012-01-30",]

pts_prev_per<-unique(pts_prev["CommunityPersonID"])

ptscomb<-merge(pts_lw_per,pts_prev_per,by=c("CommunityPersonID"),all=F)
length(unique(ptscomb$CommunityPersonID))/length(pts_lw_per$CommunityPersonID)
